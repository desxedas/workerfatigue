import javax.swing.*;

public class Login {
    private JPasswordField passwordField;
    private JTextField idField;
    private JButton loginButton;
    private JPanel loginPanel;

    public Login(){
        super();
        JFrame frame = new JFrame("Worker Fatigue");
        frame.setContentPane(loginPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String [] args){
        new Login();
    }

}
